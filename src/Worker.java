
public class Worker implements Runnable{

	//----------------------------------------------------------
	public static int computeFIB(int N){
		for(int i = 1; i <= N; i++){
			if(fibonacci(i) > N)
				return i-1;
		}
		return 0;
	}
	
	public static long fibonacci(int n) {
        if (n <= 1) return n;
        else return fibonacci(n-1) + fibonacci(n-2);
    }
	//----------------------------------------------------------
	
	public static int computeSQUARE(int N){
		for(int i = (int)Math.sqrt(N); i >= 1; i--)
			if(i*i <= N)
				return i;
		return 0;
	}
	//----------------------------------------------------------
	
	public static int computeFACT(int N){
		for(int i = N; i >= 1; i--){
			if(fact(i, N)){
				return i;
			}
		}
		return 0;
	}
	
	
	public static boolean fact(int nr, int N){
		long fact = 1;
		boolean found = true;
		for(int i = 1; i <= nr; i++){
			fact *= i;
			if(fact > N){
				found = false;
				break;				
			}
		}
		return found;
	}
	//----------------------------------------------------------
	
	public static int computePRIME(int N){
		
		for(int i = N; i > 1; i--)
			if(isPrime(i))
				return i;
		return 0;
	}
	
	public static boolean isPrime(int nr){
		boolean found = true;

		for(int  i = 2; i <= Math.sqrt(nr); i++){
			if(nr % i == 0){
				found = false;
				break;
			}
		}
		return found;		
	}
	//----------------------------------------------------------
	
	Event event;
	
	public Worker(Event event){
		this.event = event;
		
	}
	public void run() {
		
		int result;
		if(event.type.equals("PRIME")){
			result = computePRIME(event.N);
			try {
				Main.listPRIME.put(result);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if(event.type.equals("FACT")){
			result = computeFACT(event.N);
			try {
				Main.listFACT.put(result);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if(event.type.equals("SQUARE")){
			result = computeSQUARE(event.N);
			try {
				Main.listSQUARE.put(result);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
		}
		if(event.type.equals("FIB")){
			result = computeFIB(event.N);
			try {
				Main.listFIB.put(result);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
			
		
	}
}
