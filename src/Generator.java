import java.io.*;


public class Generator extends Thread{

	String fileName;
	
	public Generator(String fileName){
		this.fileName = fileName;
	}
	
	public void run(){
		String line;
		String eventType;
		int sleepTime;
		int eventN;
		try {
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			for(int i = 0; i < Main.numberOfEventsPerFile; i++){
				//read line
				line = br.readLine();
				String[] line_parts = line.split(",");
				sleepTime = Integer.parseInt(line_parts[0]);
				eventType = line_parts[1];
				eventN = Integer.parseInt(line_parts[2]);
				//sleep number of miliseconds
				sleep(sleepTime);
				//generate event
				Event newEvent = new Event(eventType, eventN);
				//add event to the queue
				Main.bq.put(newEvent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
			
	}
}
