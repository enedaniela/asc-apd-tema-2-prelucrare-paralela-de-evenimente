import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.io.PrintWriter;


public class Main {
	static int queueDim;
	static int numberOfEventsPerFile;
	//Event queue
	public static BlockingQueue<Event> bq;
	public static BlockingQueue<Integer> listPRIME;
    public static BlockingQueue<Integer> listSQUARE;
    public static BlockingQueue<Integer> listFIB;
    public static BlockingQueue<Integer> listFACT;
	
	public static void main(String[] args) {
		
		queueDim = Integer.parseInt(args[0]);
		numberOfEventsPerFile = Integer.parseInt(args[1]);

		bq = new ArrayBlockingQueue<Event>(queueDim);
		int numberOfFiles = args.length - 2;//number of files
		
		listPRIME = new ArrayBlockingQueue<Integer>(numberOfEventsPerFile * numberOfFiles);
		listSQUARE = new ArrayBlockingQueue<Integer>(numberOfEventsPerFile * numberOfFiles);
		listFIB = new ArrayBlockingQueue<Integer>(numberOfEventsPerFile * numberOfFiles);
		listFACT = new ArrayBlockingQueue<Integer>(numberOfEventsPerFile * numberOfFiles);
		
		List<Integer> PRIMEfinalList = new ArrayList<Integer>();
		List<Integer> SQUAREfinalList = new ArrayList<Integer>();
		List<Integer> FIBfinalList = new ArrayList<Integer>();
		List<Integer> FACTfinalList = new ArrayList<Integer>();
		
		final int numThreads = numberOfFiles;
		Generator generators[] = new Generator[numThreads];
		
		for(int i = 0 ; i < numThreads ; i++){
			generators[i] = new Generator(args[i + 2]);
		}
		for(int i = 0 ; i < numThreads ; i++){
			generators[i].start();
		}
					
		
		
		ExecutorService tpe = Executors.newFixedThreadPool(4);
		//Compute all events
		for(int i = 0; i < numberOfEventsPerFile * numberOfFiles; i++){
			//Take one event from the queue and submit it to the worker's pool
			try {
				tpe.submit(new Worker(bq.take()));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
		//Shutdown the ExecutorService because all events have been processed
		tpe.shutdown();
		try {
			tpe.awaitTermination(100, TimeUnit.DAYS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		for(int i = 0 ; i < numThreads; ++i)
			try {
				generators[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		

		PRIMEfinalList.addAll(listPRIME);
		SQUAREfinalList.addAll(listSQUARE);
		FIBfinalList.addAll(listFIB);
		FACTfinalList.addAll(listFACT);
		
		//Sort the lists
		Collections.sort(PRIMEfinalList);
		Collections.sort(SQUAREfinalList);
		Collections.sort(FIBfinalList);
		Collections.sort(FACTfinalList);
		
		//Write the lists to output files
		try{
		    PrintWriter writer = new PrintWriter("PRIME.out", "UTF-8");
		    for(int i = 0; i < PRIMEfinalList.size(); i++)
		    	writer.println(PRIMEfinalList.get(i));
		    writer.close();
		} catch (Exception e) {
		   System.out.println("Can't write to file PRIME.out");
		}
		
		try{
		    PrintWriter writer1 = new PrintWriter("FACT.out", "UTF-8");
		    for(int i = 0; i < FACTfinalList.size(); i++)
		    	writer1.println(FACTfinalList.get(i));
		    writer1.close();
		} catch (Exception e) {
		   System.out.println("Can't write to file FACT.out");
		}
		
		try{
		    PrintWriter writer = new PrintWriter("SQUARE.out", "UTF-8");
		    for(int i = 0; i < SQUAREfinalList.size(); i++)
		    	writer.println(SQUAREfinalList.get(i));
		    writer.close();
		} catch (Exception e) {
		   System.out.println("Can't write to file SQUARE.out");
		}
		
		try{
		    PrintWriter writer = new PrintWriter("FIB.out", "UTF-8");
		    for(int i = 0; i < FIBfinalList.size(); i++)
		    	writer.println(FIBfinalList.get(i));
		    writer.close();
		} catch (Exception e) {
		   System.out.println("Can't write to file FIB.out");
		}
	}
}
